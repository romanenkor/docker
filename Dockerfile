FROM archlinux:latest

RUN pacman -Syyu --noconfirm && pacman -S \
        git gcc clang cmake

CMD [ "/bin/bash" ]
